#!/usr/bin/env bats

teardown () {
  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "push to registry, use image in pod" {
  export TAG=$(date +%s)
  export IMAGE="{{ .Values.domain }}/library/harbor-inspect-test:${TAG}"

  run skopeo --debug login --password "{{ .Values.password }}" --username "{{ .Values.username }}" "{{ .Values.domain }}"
  [ $status -eq 0 ]

  run skopeo --debug login --get-login "{{ .Values.domain }}"
  [ $status -eq 0 ]

  run skopeo --debug copy docker://alpine:3.17.0 "docker://${IMAGE}"
  [ $status -eq 0 ]

  run skopeo --debug inspect "docker://${IMAGE}"
  [ $status -eq 0 ]
}
